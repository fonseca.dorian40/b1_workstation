#Fonseca Dorian 01-11-2020
#Script listant plusieurs informations sur la machine.

""
echo "Last device boot up"
(Get-CimInstance Win32_OperatingSystem).LastBootUpTime
""
echo "The device name is :"  
[system.environment]::MachineName
""
echo "The device Users are :"
net user 
""
echo "The device main IP is :"
$env:HostIP = ( `
    Get-NetIPConfiguration | `
    Where-Object { `
        $_.IPv4DefaultGateway -ne $null `
    } `
).IPv4Address.IPAddress
echo $env:HostIP
""
echo "Total Ram :"
get-ciminstance -class "cim_physicalmemory" | % {$_.Capacity}
$freeram = Get-CIMInstance Win32_OperatingSystem | Select FreePhysicalMemory
echo $freeram
""
echo "The OS and his Version :"
(Get-WmiObject Win32_OperatingSystem).Caption
(Get-WmiObject Win32_OperatingSystem).Version
""
echo "average ping latency to 8.8.8.8 server :"
$8888 = (ping 8.8.8.8)
echo $8888
""
echo "Total Disk memory"
(Get-WmiObject Win32_LogicalDisk).Size
echo "Free Disk memory"
(Get-WmiObject Win32_LogicalDisk).FreeSpace
""