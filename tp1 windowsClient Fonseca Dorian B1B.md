# tp1

## I. Self-footprinting

### Host OS

#### Déterminer les principales informations de votre machine

* nom de la machine
```
$ Get-WmiObject Win32_OperatingSystem CSName
[...]
CSName           : LAPTOP-HBUG3T8J
[...]
```
* OS et version
```
$ Get-WmiObject Win32_OperatingSystem caption
[...]
Caption          : Microsoft Windows 10 Famille
[...]
```
```
$ Get-WmiObject Win32_OperatingSystem Version
[...]
Version          : 10.0.18363
[...]
```
* architecture processeur (32-bit, 64-bit, ARM, etc)
```
$ Get-ComputerInfo
[...]
CsSystemType         : x64-based PC
[...]
```
* quantité RAM et modèle de la RAM
```
$ Get-CimInstance win32_physicalmemory
[...]
PartNumber           : ACR26D4S9S8ME-8
[...]
BankLabel            : BANK 2
Capacity             : 8589934592
[...]
```
### Devices
#### Trouver
* la marque et le modèle de votre processeur
    * identifier le nombre de processeurs, le nombre de coeur
        ```
        $ WMIC CPU Get DeviceID,NumberOfCores,NumberOfLogicalProcessors
        DeviceID  NumberOfCores  NumberOfLogicalProcessors
        CPU0      4              8
        ```
    * si c'est un proc Intel, expliquer le nom du processeur
       ```
       Intel(R) Core(TM) i5-8300H CPU @ 2.30GHz
       
       Intel(R) Core(TM) : Marque du constructeur
       i5-8300H : Version du processeur
       CPU @ 2.30GHz : Processeur de 2.30GHz de cadence
       ```
* la marque et le modèle
    * de votre touchpad/trackpad
        ```
        Aucune recherche concluante concernant le touchpad de la machine
        ```
    * de votre carte graphique
        ```
        $ Get-WmiObject Win32_VideoController
        [...]
        VideoProcessor               : GeForce GTX 1050 Ti
        [...]
        ```

#### Disque Dur
* identifier la marque et le modèle de votre(vos) disque(s) dur(s)
    ```
    $ Get-disk | fl
    [...]
    Model              : HGST HTS721010A9E630
    Model              : INTEL SSDPEKKW256G8
    [...]
    ```
* identifier les différentes partitions de votre/vos disque(s) dur(s)
    ```
    $ get-partition
    
    PartitionNumber  DriveLetter Offset                                        Size Type
    1                D           1048576                                  931.51 GB Basic


   DiskPath : \\?\scsi#disk&ven_nvme&prod_intel_ssdpekkw25#4&28090cb5&0&010000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

    PartitionNumber  DriveLetter Offset                                        Size Type

    1                            1048576                                     100 MB System
    2                            105906176                                    16 MB Reserved
    3                C           122683392                                237.35 GB Basic
    4                            254978031616                                  1 GB Recovery
    ```
* déterminer le système de fichier de chaque partition
    ```
    $ diskpart
    ```
    Puis sur diskpart.exe 
    ```
    $ select disk 0
    [...]
    $ list volume 
    
      N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 0     D   DATA         NTFS   Partition    931 G   Sain
  Volume 1     C   Acer         NTFS   Partition    237 G   Sain       Démarrag
  Volume 2         ESP          FAT32  Partition    100 M   Sain       Système
    ```
* expliquer la fonction de chaque partition
```
    System : Parti utilise par windows pour le demarage.
    Reserved : Parti creer par le systeme lors de son instalation, utilise differemment suivant la version de celui-ci.
    Basic : Parti restante des trois autres, disponible a l'utilisateur.
    Recovery : Parti utlise pour la recuperation du disques.
```

### Users

#### Déterminer la liste des utilisateurs de la machine

* la liste complète des utilisateurs de la machine
```
$ Get-CimInstance -ClassName Win32_Desktop

SettingID Name                 ScreenSaverActive ScreenSaverSecure ScreenSaverTimeout
--------- ----                 ----------------- ----------------- ------------------
          AUTORITE NT\Système  False
          LAPTOP-HBUG3T8J\User False
          .DEFAULT             False
```
* déterminer le nom de l'utilisateur qui est full admin sur la machine
    Il s'agit de l'utilisateur AUTORITE NT/Systeme. Cet utilisateur système intégré à Windows possèdent des permissions full admin.
    
### Processus

#### Déterminer la liste des processus de la machine

* je vous épargne l'explication de chacune des lignes
    ```
    $ Get-Process
    
    Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id     SI ProcessName
    915      45        69908      13428     2,23       10832  2  ACCStd
    [...]
    511      31        45200      31004                4528   0  XtuService
    696      47        24988      16380     0,30       9560   2  YourPhone
    ```    
    
* choisissez 5 services système et expliquer leur utilité

    WmiPrvSE : C'est un processus générique permettant la gestion des clients sous Windows. Il est initialisé à la première connexion d'une application cliente et permet de surveiller des ressources du système. Il s'agit d'un processus essentiel du système ne pouvant être arrêté.
    
    WWAHost : WWAHost.exe est un processus sécurisé et essentiel dans Windows. Il agit en tant qu'hôte pour les processus d'application Metro, de la même manière que svchost.exe agit en tant qu'hôte pour les fichiers .dll. Ce processus est sûr et il est préférable de laisser seul le système pour continuer à fonctionner normalement sur un système Windows. (Il était précisé qu'il était essentiel sous windows 8).
    
    svchost : svchost.exe (Service Host ou SvcHost) littéralement traduit par l'hôte de service, est l’un des plus importants processus Windows associé avec les services de systèmes Windows dans ses multiples versions. Il signifie « Service Host Process » et sert d'hôte pour les fonctionnalités de bibliothèques de liens dynamiques (DLL).
    
    csrss : Il s'agit du processus d'exécution client/serveur de windows. Il permet de notamment de gérer le mode utilisateur de Windows.
    
    winlogon : Processus windows servant à ouvrir et fermer les sessions windows.
    
### Network

#### Afficher la liste des cartes réseau de votre machine

* expliquer la fonction de chacune d'entre 
```
$ Get-NetAdapter OU $ ipconfig

Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
Connexion réseau Bluet... Bluetooth Device (Personal Area Netw...      14 Disconnected 48-A4-72-43-D7-**         3 Mbps
Wi-Fi                     Intel(R) Wireless-AC 9560 160MHz              5 Up           48-A4-72-43-D7-**       585 Mbps
Ethernet                  Realtek PCIe GbE Family Controller            2 Disconnected 98-28-A6-27-EB-**          0 bps
```

La carte Wi-Fi : Elle sert à connecté un appareil à un réseaux sans fil.
La carte Ethernet : Elle sert à connecté un appareil à un réseaux avec fil.
La carte Bluetooth : Elle sert à connecté un appareil à un réseaux sans fil sur une très courte distance.

#### Lister tous les ports TCP et UDP en utilisation

```
$ netstat -ano 
TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       1112
[...]
UDP    [fe80::b480:a6b7:6412:8f59%5]:2177   *:*        7396
UDP    [fe80::b480:a6b7:6412:8f59%5]:50144  *:*        12400
```

* déterminer quel programme tourne derrière chacun des ports
    
```
$ tasklist

[...]
svchost.exe                   1112 Services                   0    16 864 Ko
[...]
```

* expliquer la fonction de chacun de ces programmes
    La commande ``` tasklist ``` permet de lister tout les programmes actifs sur la machine. Le progamme svchost.exe, que l'on retrouve dans la liste des programmes, des ports mais aussi parmis les services systèmes essentiels, permet de servir d'hôtes aux fonctionnalitées DLL.
    
## II. Scripting

###  Utiliser un langage de scripting natif à votre OS

* trouvez un langage natif par rapport à votre OS
    Le langage natif de mon OS (windows) est le powershell.
* l'utiliser pour coder un script qui
    * affiche un résumé de l'OS


## III. Gestion de softs

* Expliquer l'intérêt de l'utilisation d'un gestionnaire de paquets
```
Un gestionnaire de paquets permet de facilite l'installation de nouveaux programmes, leur mise à jour ou leur suppression. Il permet donc d'éviter la corruption ou la perte de fichiers lors d'un téléchargement ainsi que de simplifié et sécurisé cette tâche.
```
* Utiliser un gestionnaire de paquets propres à votre OS.

Les paquets dejà installé :
```
$ choco list --local-only
Chocolatey v0.10.15
chocolatey 0.10.15
1 packages installed.
```

Lors de l'instalation d'un logiciel type googlechrome avec cette commande : 
```
$ choco install googlechrome
[...]
 from 'https://dl.google.com/tag/s/dl/chrome/install/googlechromestandaloneenterprise64.msi'
[...]
```
On peut voir que Choco laisse google trouver le liens de téléchargement.

## IV. Machine Virtuel

### 3.Configuration Post-Install
```
$ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:8d:6d:4f brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 81374sec preferred_lft 81374sec
    inet6 fe80::277d:587b:7dc7:3d6d/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:e4:a2:54 brd ff:ff:ff:ff:ff:ff
    inet 192.168.120.50/24 brd 192.168.120.255 scope global noprefixroute dynamic enp0s8
       valid_lft 348sec preferred_lft 348sec
    inet6 fe80::abb8:e40d:3afc:f8b7/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
```
$ssh root@192.168.120.50
The authenticity of host '192.168.120.50 (192.168.120.50)' can't be established.
ECDSA key fingerprint is SHA256:LBIbCLFxh+mKTe8p0zlUSTTr5Tug0RKjUKQ+fldMWgY.
ECDSA key fingerprint is MD5:12:ec:e2:3a:0c:7d:7e:70:8a:e7:74:74:81:fc:8a:16.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '192.168.120.50' (ECDSA) to the list of known hosts.
root@192.168.120.50's password:
Last login: Mon Nov  9 12:04:11 2020 from 1
```

### 4.Partage de Fichier

```
$ yum install -y cifs-utils
$ mkdir C:\VM
$ sudo mount -t cifs -o username=User,password= . . . . . . . . //192.168.120.1/VM C:\VM
$ ls
anaconda-ks.cfg  C:VM  Les  Liste
```